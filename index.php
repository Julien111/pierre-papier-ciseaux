<?php

/*
*Si $ POST est vide
connexion au serveur du shi fou mi
Serveur dit bonjour à l'invité
Serveur envoit la liste des choix possible pour l'utilisateur
invité choisie et valide sa carte

*Si $ POST n'est pas vide
Traîter les informations reçues par l'utilisateur et les valeurs cachés pour voir où on en est.
*/

(array) $ordinateur = array("ciseaux" => "ciseaux", "pierre" =>"pierre", "papier" =>"papier");

(string) $ciseau = "ciseau";
(string) $pierre = "pierre";
(string) $papier = "papier";

(int) $score_joueur = 0;
(int) $score_ordinateur = 0;   

//les informations renvoyées par ($POST['choix-joueur'], score_joueur, score_ordinateur);
//

if(!empty($_POST['choix_joueur'])){

    $choix_ordinateur = array_rand($ordinateur);
    (int) $score_joueur = intval($_POST['score_joueur']);
    (int) $score_ordinateur = intval($_POST['score_ordinateur']);;  

    if(!empty($_POST["choix_joueur"])){
        
        $choix_joueur = $_POST["choix_joueur"];
        $result = "";
        
        if($choix_joueur === $choix_ordinateur){
            $result ="Egalité";
        }
        else if("pierre" === $choix_joueur && "papier" === $choix_ordinateur){
            $result ="Perdu !";
            $score_ordinateur++;
        }
        else if ("pierre" == $choix_joueur && "ciseaux" === $choix_ordinateur){
            $result ="Gagné !";
            $score_joueur++;
        }
        else if("papier" === $choix_joueur && "pierre" === $choix_ordinateur){
            $result ="Gagné !";
            $score_joueur++;
        }
        else if("papier" === $choix_joueur && "ciseau" === $choix_ordinateur){
            $result ="Perdu !";
            $score_ordinateur++;
        }
        else if("ciseaux" === $choix_joueur && "papier" === $choix_ordinateur){
            $result ="Gagné !";
            $score_joueur++;
        }
        else{
            $result = 'Perdu !';
            $score_ordinateur++;
        }
    }
}
else
{

    (int) $score_joueur = 0;
    (int) $score_ordinateur = 0;   
}
?>

<!DOCTYPE html>


<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <title>Jeu</title>
</head>
<body>
    <div class='container'>
        <h1>Le jeu : pierre, papier, ciseaux</h1>
    
        <p class="para">La partie se déroule en trois manches gagnantes. Les égalités ne sont pas prises en compte.</p>
        <ul>
            <li>La pierre écrase les ciseaux et gagne.</li>
            <li>La papier enveloppe la pierre et gagne.</li>
            <li>Les ciseaux découpent la feuille et gagnent.</li>
        
        </ul> 
        
        <p class="para">A partir de là chaque forme en bat une autre et perd contre une autre.</p>


        <p id="color">Score joueur :   <?php  printf('%s', $score_joueur) ?></p>
        <p id="color2">Score ordinateur : <?php printf('%s', $score_ordinateur) ?></p>

         <?php

        if(!empty($result)){?>
             <p id='result'><?php printf($result)?></p>

        <?php
        }
        ?>
        <?php 

            if($score_joueur < 3 && $score_ordinateur < 3 ):
        ?>

        <form method='post' action="index.php">

            <p><label for='choix'>Choisissez entre pierre, papier ou ciseaux ?</label></p>

            <div id="blocGeneral">

                <div id='bloc1'>
                    <img src='images/rock.png' width='100px'>
                    <p><input type="radio" name='choix_joueur' value='pierre' required id='pierre' /></p>
                </div>

                <div id='bloc2'>
                    <img src='images/papier.png' id='papier' width='100px'>
                    <p><input type="radio" name='choix_joueur' value='papier' required id='papier' /></p>
                </div>

                <div id='bloc3'>
                    <img src='images/ciseau.png' alt='ciseaux' width='100px'/>
                    <p><input type="radio" name='choix_joueur' value='ciseaux' required id='ciseaux' /></p>
                </div>

            </div>

            <input type='hidden' name='score_joueur' value=<?php printf('%s', $score_joueur)?>/>
            <input type='hidden' name='score_ordinateur' value=<?php printf('%s', $score_ordinateur)?>/>
            
            <p><input id='btn' type='submit' value='Valider' /></p>

            
        </form>

        

         <?php 
        else:
                    
        ?>

        <h6>Fin de Partie.</h6>
        <h6 id='message_fin'><?php if($score_ordinateur === 3){ print("L'ordinateur a gagné la partie");}else{ print("Le joueur a gagné la partie");}?></h6>
            
        <form action="index.php" method="post">
            <input type="submit" name='refresh' id="refresh" value="Nouvelle partie" />

            <?php if(isset($_POST['refresh']) AND $_POST['refresh']=='actualiser') {
                $score_joueur = 0;
                $score_ordinateur = 0;
            }
            
            
            ?>
        </form>
        <?php 

        endif;
        ?>

    </div>

    <script src='js/script.js'></script>
</body>
</html>


<?php
